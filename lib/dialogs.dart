import 'package:flutter/material.dart';
import 'bottom_sheet.dart' as bts;

abstract class BottomSheetDialogs {
  static Future<void> generic({
    required BuildContext context,
    required String content,
    required String title,
    required Widget icon,
    List<Widget> buttons = const [],
    Widget? button,
    String? routeTo,
    VoidCallback? onPress,
    String? btnLabel,
  }) async {
    return await bts.make(
      context,
      BaseDialog(
        title: title,
        content: content,
        icon: icon,
        routeTo: routeTo,
        onPress: onPress,
        buttons: buttons,
        button: button,
      ),
    );
  }

  static Future<void> fail({
    required BuildContext context,
    required String content,
    Widget? icon,
    String? routeTo,
    VoidCallback? onPress,
    String? btnLabel,
    String? title,
    Widget? button,
  }) async {
    await bts.make(
      context,
      BaseDialog(
        icon: icon ?? Icon(Icons.warning_rounded, size: 80),
        title: title ?? 'Falha!',
        btnLabel: btnLabel,
        content: content,
        routeTo: routeTo,
        onPress: onPress,
        button: button,
      ),
    );
  }

  static Future<void> warning({
    required BuildContext context,
    required String content,
    Widget? icon,
    String? routeTo,
    VoidCallback? onPress,
    String? btnLabel,
    String? title,
    Widget? button,
  }) async {
    return await bts.make(
      context,
      BaseDialog(
        icon: icon ?? Icon(Icons.warning_rounded, size: 80),
        title: title ?? 'Atenção!',
        btnLabel: btnLabel,
        content: content,
        routeTo: routeTo,
        onPress: onPress,
        button: button,
      ),
    );
  }

  static Future<void> success({
    required BuildContext context,
    required String content,
    Widget? icon,
    String? routeTo,
    VoidCallback? onPress,
    String? btnLabel,
    String? title,
    Widget? button,
  }) async {
    return await bts.make(
      context,
      BaseDialog(
        icon: icon ?? Icon(Icons.done_rounded, size: 80),
        title: title ?? 'Sucesso!',
        btnLabel: btnLabel,
        content: content,
        routeTo: routeTo,
        onPress: onPress,
        button: button,
      ),
    );
  }
}

class BaseDialog extends StatelessWidget {
  const BaseDialog({
    Key? key,
    required this.title,
    required this.content,
    required this.icon,
    this.routeTo,
    this.onPress,
    this.btnLabel,
    this.button,
    this.buttons = const [],
  }) : super(key: key);

  final Widget icon;
  final Widget? button;
  final String title;
  final String content;
  final String? routeTo;
  final String? btnLabel;
  final VoidCallback? onPress;
  final List<Widget> buttons;

  @override
  Widget build(BuildContext context) {
    _onPressed() {
      if (onPress != null) {
        onPress!();
      } else {
        routeTo == null
            ? Navigator.pop(context)
            : Navigator.pushReplacementNamed(context, routeTo!);
      }
    }

    _buildButtons() {
      if (buttons.length > 0) {
        return Row(children: buttons);
      }
      if (button == null) {
        return ElevatedButton(
          child: Text('OK'),
          onPressed: _onPressed,
        );
      }
      return button!;
    }

    return Container(
      padding: EdgeInsets.fromLTRB(24, 0, 24, 24),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: 100, child: icon),
          Padding(
            padding: EdgeInsets.all(25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(title, style: Theme.of(context).textTheme.headlineMedium),
                SizedBox(height: 12),
                Text(content, style: Theme.of(context).textTheme.bodyMedium),
              ],
            ),
          ),
          _buildButtons()
        ],
      ),
    );
  }
}
