import 'package:flutter/material.dart';

class GradientButton extends StatelessWidget {
  const GradientButton(this.label,this.onPress,{this.gradient, Key? key}) : super(key: key);

  final String label;
  final VoidCallback onPress;
  final Gradient? gradient;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          gradient: gradient ?? LinearGradient(
            colors: [Color(0xFF000000),Color(0xFF434343)],
            begin: Alignment.bottomCenter,
            end: Alignment.topCenter,
          )
      ),
      constraints: BoxConstraints(minHeight: 56),
      child: TextButton(
        onPressed: () => onPress(),
        child: Text(label, style: Theme.of(context).textTheme.button,),
      ),
    );
  }
}


