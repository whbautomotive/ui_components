import 'package:flutter/material.dart';

dynamic make(context, Widget child,
    {double maxHeight = 525,
    bool? enableDrag,
    bool? isDismissible,
    BoxConstraints? constraints,
    bool? isScrollControlled,
    Color? barrierColor,
    Clip? clipBehavior,
    double? elevation,
    RouteSettings? routeSettings,
    AnimationController? transitionAnimationController,
    bool? useRootNavigator}) async {
  return await showModalBottomSheet(
    context: context,
    backgroundColor: Theme.of(context).bottomSheetTheme.backgroundColor,
    shape: Theme.of(context).bottomSheetTheme.shape,
    enableDrag: enableDrag ?? true,
    isDismissible: isDismissible ?? true,
    constraints: constraints,
    isScrollControlled: isScrollControlled ?? false,
    barrierColor: barrierColor,
    clipBehavior: clipBehavior,
    elevation: elevation,
    routeSettings: routeSettings,
    transitionAnimationController: transitionAnimationController,
    useRootNavigator: useRootNavigator ?? false,
    builder: (context) {
      return Container(
        constraints: BoxConstraints(maxHeight: maxHeight),
        child: Padding(
          padding:
              MediaQuery.of(context).viewInsets.add(EdgeInsets.only(top: 48)),
          child: child,
        ),
      );
    },
  );
}
